package com.wwsight.bigbank;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NfcAdapter;
import android.nfc.tech.IsoDep;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ingenico.nfcreader.CardAnalyst;
import com.ingenico.nfcreader.ReaderNfc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


public class Tapcard extends Activity {

    /* -------- NFC Reader variables -------*/
    final private String Tag = "readernfc" ;
    private NfcAdapter mNfcAdapter;
    private static final IntentFilter[] INTENT_FILTER = new IntentFilter[] { new IntentFilter(NfcAdapter.ACTION_TECH_DISCOVERED) };
    private static final String[][] TECH_LIST = new String[][] { { IsoDep.class.getName() } };
    PendingIntent mPendingIntent ;
    //--------------------------------------*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tapcard);

        Intent myIntent = getIntent();
        String price = myIntent.getStringExtra("price");
        String nameApp = myIntent.getStringExtra("nameApp");
        String comments = myIntent.getStringExtra("comments");
        String[] products = myIntent.getStringArrayExtra("products");

        TextView priceText = (TextView) findViewById(R.id.price);
        TextView nameAppText = (TextView) findViewById(R.id.nameApp);
        TextView commentsText = (TextView) findViewById(R.id.comments);

        priceText.setText(price+" $");
        nameAppText.setText(nameApp);
        commentsText.setText(comments);

        ImageView x = (ImageView) this.findViewById(R.id.X);

        x.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(Bigbank.this,"BRAVO", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(), Bigbank.class));
                overridePendingTransition(R.anim.slide_in_from_top, R.anim.slide_out_to_bottom);
            }
        });

        ImageView ok = (ImageView) this.findViewById(R.id.ok);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(Bigbank.this,"BRAVO", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(), ChooseColor.class));
                overridePendingTransition(R.anim.slide_in_from_top, R.anim.slide_out_to_bottom);
            }
        });

        /*----- NFC reader intent creation ---*/
        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
        mPendingIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, this.getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        //------------------------------------*/

    }

    protected void onPause(){
        super.onPause();
        if (mNfcAdapter != null) {
            mNfcAdapter.disableForegroundDispatch(this);
        }
    }

    protected void onResume() {
        super.onResume() ;
        if (mNfcAdapter != null) {
            mNfcAdapter.enableForegroundDispatch(this, mPendingIntent, INTENT_FILTER, TECH_LIST);
        }
    }

    /*----- NFC get action ------*/
    protected void onNewIntent(final Intent intent) {
        ReaderNfc sender = new ReaderNfc(intent) ;

        HashMap<Integer, byte[]> card = CardAnalyst.readCard(sender) ;

        byte bpan[] = card.get(0x5A) ; // PAN
        String pan = "";
        if(bpan != null){
            pan = CardAnalyst.hextostring(bpan) ;
        }else{
            byte biso2[] = card.get(0x57) ; // Pan from ISO 2
            if (biso2 != null){
                biso2 = Arrays.copyOfRange(biso2, 0, 8) ;
                pan = CardAnalyst.hextostring(biso2) ;
            }
        }

        String dateexp = "";
        byte bdatexp[] =  card.get(0x5F24) ; // Expiration Date
        if(bdatexp != null){
            dateexp =  CardAnalyst.hextostring(bdatexp).replace(" ", "/").substring(0, 8) ;
        }

        String crytogram = "";
        byte bcrytogram[] =  card.get(0x9F26) ; // Application cryptogramme
        if(bcrytogram != null){
            crytogram = CardAnalyst.hextostring(bcrytogram) ;
        }


        System.out.println("PAN : " + pan ) ;
        System.out.println("Date Expiration : " + dateexp ) ;
        System.out.println("Application cryptogram : " + crytogram ) ;


        startActivity(new Intent(getApplicationContext(), ChooseColor.class));
        //overridePendingTransition(R.anim.slide_in_from_top, R.anim.slide_out_to_bottom);

    }
}
