package com.wwsight.bigbank;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;


public class ChooseColor extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_color);


        ImageView x = (ImageView) this.findViewById(R.id.X);

        x.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(Bigbank.this,"BRAVO", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(), Bigbank.class));
                overridePendingTransition(R.anim.slide_in_from_top, R.anim.slide_out_to_bottom);
            }
        });


        View rose = (View) this.findViewById(R.id.roseColor);

        rose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(ChooseColor.this, "je suis ChooseColor", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(getApplicationContext(), Grille.class);
                i.putExtra("color", "#f0308f");
                startActivity(i);
               overridePendingTransition(R.anim.slide_in_from_top, R.anim.slide_out_to_bottom);
            }
        });

        View vert = (View) this.findViewById(R.id.vertColor);

        vert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(ChooseColor.this, "je suis ChooseColor", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(getApplicationContext(), Grille.class);
                i.putExtra("color", "#2de56d");
                startActivity(i);
                overridePendingTransition(R.anim.slide_in_from_top, R.anim.slide_out_to_bottom);
            }
        });

        View bleu = (View) this.findViewById(R.id.bleuColor);

        bleu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(ChooseColor.this, "je suis ChooseColor", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(getApplicationContext(), Grille.class);
                i.putExtra("color", "#1e56f3");
                startActivity(i);
                overridePendingTransition(R.anim.slide_in_from_top, R.anim.slide_out_to_bottom);
            }
        });

        View jaune = (View) this.findViewById(R.id.jauneColor);

        jaune.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(ChooseColor.this, "je suis ChooseColor", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(getApplicationContext(), Grille.class);
                i.putExtra("color", "#e1a729");
                startActivity(i);
                overridePendingTransition(R.anim.slide_in_from_top, R.anim.slide_out_to_bottom);
            }
        });



    }

}
