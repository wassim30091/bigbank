package com.wwsight.bigbank;

import android.app.Activity;
import android.os.Bundle;
import android.content.Intent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;


public class Bigbank extends Activity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_bigbank);


        LinearLayout transf = (LinearLayout) this.findViewById(R.id.menu1);

        transf.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Toast.makeText(Bigbank.this, "BRAVO", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(), Transfers.class));
                overridePendingTransition(R.anim.slide_in_from_bottom, R.anim.slide_out_to_top);
            }
        });


        LinearLayout pay = (LinearLayout) this.findViewById(R.id.menu2);

        pay.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Toast.makeText(Bigbank.this,"BRAVO", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(), Payments.class));
                overridePendingTransition(R.anim.slide_in_from_bottom, R.anim.slide_out_to_top);
            }
        });

        LinearLayout menu3 = (LinearLayout) this.findViewById(R.id.menu3);

        menu3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Toast.makeText(Bigbank.this,"BRAVO", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(), Facture.class));
                overridePendingTransition(R.anim.slide_in_from_bottom, R.anim.slide_out_to_top);
            }
        });

        LinearLayout menu4 = (LinearLayout) this.findViewById(R.id.menu4);

        menu4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Toast.makeText(Bigbank.this,"BRAVO", Toast.LENGTH_SHORT).show();
                //startActivity(new Intent(Bigbank.this, Mouvements.class));
                // overridePendingTransition(R.anim.slide_in_from_bottom, R.anim.slide_out_to_top);
            }
        });

        ImageView aide = (ImageView) this.findViewById(R.id.aide);

        aide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(Bigbank.this,"BRAVO", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(), Aide.class));
                overridePendingTransition(R.anim.slide_in_from_bottom, R.anim.slide_out_to_top);
            }
        });

        ImageView profil = (ImageView) this.findViewById(R.id.prof);

        profil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(Bigbank.this,"BRAVO", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(), Profil.class));
                overridePendingTransition(R.anim.slide_in_from_bottom, R.anim.slide_out_to_top);
            }
        });

    }


}
