package com.wwsight.bigbank.Activity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.wwsight.bigbank.Jobs.InformationsCart;
import com.wwsight.bigbank.R;

import java.util.ArrayList;
import java.util.HashMap;

public class Historique extends Activity {

    // List view
    private ListView lv;

    // Listview Adapter
    ArrayAdapter<String> adapter;

    // ArrayList for Listview
    ArrayList<HashMap<String, String>> productList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historique);

        // Listview Data
        String action[] = {"Paiement : MEISO.FR", "Paiement : MEISO.FR"};

        lv = (ListView) findViewById(R.id.list_view);


        // Adding items to listview
        adapter = new ArrayAdapter<String>(this, R.layout.list_item_histry, R.id.action, action);
        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,
                                    int position, long arg3) {
                // TODO Auto-generated method stub

                // ListView Clicked item value
                String itemValue = (String) lv.getItemAtPosition(position);

                //When clicked evrywhere, open prologue.java
                Intent i = new Intent(getApplicationContext(), TapcardActivity.class);
                InformationsCart.setNameApp(itemValue);
                startActivity(i);
                overridePendingTransition(R.anim.hold, R.anim.fade);

            }
        });

    }

}


