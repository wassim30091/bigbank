package com.wwsight.bigbank.Activity;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.wwsight.bigbank.Jobs.InformationsCart;
import com.wwsight.bigbank.R;

public class Search extends Activity {

    // List view
    private ListView lv;

    // Listview Adapter
    ArrayAdapter<String> adapter;

    // Search EditText
    EditText inputSearch;


    // ArrayList for Listview
    ArrayList<HashMap<String, String>> productList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        ImageView retour = (ImageView) this.findViewById(R.id.retour);

        retour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Toast.makeText(BigbankActivity.this,"pas encore", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(), TransfersActivity.class));
                overridePendingTransition(R.anim.slide_in_from_bottom, R.anim.slide_out_to_top);
            }
        });



        // Listview Data
        String contacts[] = {"Adriana BBRECHO", "Adrian ECHALLE", "Alain BERTHOZ", "Albert RISOLUTTI", "Alice LORK",
                "Anne SURMONT", "Bastien BURANOVA",
                "Ben ANTIER", "Brahim LEVI", "Camille CHEZE", "Cesar ATTIA"};

        lv = (ListView) findViewById(R.id.list_view);
        inputSearch = (EditText) findViewById(R.id.inputSearch);

        // Adding items to listview
        adapter = new ArrayAdapter<String>(this, R.layout.list_item, R.id.contacts_name, contacts);
        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,
                                    int position, long arg3) {
                // TODO Auto-generated method stub

                // ListView Clicked item value
                String  itemValue    = (String) lv.getItemAtPosition(position);

                //When clicked evrywhere, open prologue.java
                Intent i = new Intent(getApplicationContext(), TapcardActivity.class);
                InformationsCart.setNameApp(itemValue);
                startActivity(i);
                overridePendingTransition(R.anim.hold, R.anim.fade);

            }
        });
                /**
                 * Enabling Search Filter
                 * */
                inputSearch.addTextChangedListener(new TextWatcher() {

                    @Override
                    public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                        // When user changed the Text
                        Search.this.adapter.getFilter().filter(cs);
                    }

                    @Override
                    public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                                  int arg3) {
                        // TODO Auto-generated method stub

                    }

                    @Override
                    public void afterTextChanged(Editable arg0) {
                        // TODO Auto-generated method stub
                    }
                });
            }
        }