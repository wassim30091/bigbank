package com.wwsight.bigbank.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.wwsight.bigbank.Jobs.InformationsCart;
import com.wwsight.bigbank.R;


public class ChooseColorActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_color);

        TextView priceText = (TextView) findViewById(R.id.price);
        TextView nameAppText = (TextView) findViewById(R.id.nameApp);
        TextView commentsText = (TextView) findViewById(R.id.comments);

        priceText.setText(InformationsCart.getTotalPrice()+" $");
        nameAppText.setText(InformationsCart.getNameApp());
        commentsText.setText(InformationsCart.getComments());

        ImageView x = (ImageView) this.findViewById(R.id.X);

        x.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(BigbankActivity.this,"BRAVO", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(), BigbankActivity.class));
                overridePendingTransition(R.anim.slide_in_from_top, R.anim.slide_out_to_bottom);
            }
        });


        View rose = (View) this.findViewById(R.id.roseColor);

        rose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(ChooseColorActivity.this, "je suis ChooseColorActivity", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(getApplicationContext(), GrilleActivity.class);
                i.putExtra("color", "#f0308f");
                startActivity(i);
                overridePendingTransition(R.anim.fade, R.anim.zoom_in);
            }
        });

        View vert = (View) this.findViewById(R.id.vertColor);

        vert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(ChooseColorActivity.this, "je suis ChooseColorActivity", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(getApplicationContext(), GrilleActivity.class);
                i.putExtra("color", "#2de56d");
                startActivity(i);
                overridePendingTransition(R.anim.fade, R.anim.zoom_in);
            }
        });

        View bleu = (View) this.findViewById(R.id.bleuColor);

        bleu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(ChooseColorActivity.this, "je suis ChooseColorActivity", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(getApplicationContext(), GrilleActivity.class);
                i.putExtra("color", "#1e56f3");
                startActivity(i);
                overridePendingTransition(R.anim.fade, R.anim.zoom_in);
            }
        });

        View jaune = (View) this.findViewById(R.id.jauneColor);

        jaune.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(ChooseColorActivity.this, "je suis ChooseColorActivity", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(getApplicationContext(), GrilleActivity.class);
                i.putExtra("color", "#e1a729");
                startActivity(i);
                overridePendingTransition(R.anim.fade, R.anim.zoom_in);
            }
        });

    }

}
