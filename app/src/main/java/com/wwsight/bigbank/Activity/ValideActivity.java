package com.wwsight.bigbank.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.wwsight.bigbank.Jobs.InformationsCart;
import com.wwsight.bigbank.R;


public class ValideActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_valide);

        TextView priceText = (TextView) findViewById(R.id.price);
        TextView nameAppText = (TextView) findViewById(R.id.nameApp);
        TextView commentsText = (TextView) findViewById(R.id.comments);

        priceText.setText(InformationsCart.getTotalPrice()+" $");
        nameAppText.setText(InformationsCart.getNameApp());
        commentsText.setText(InformationsCart.getComments());

        ImageView x = (ImageView) this.findViewById(R.id.X);

        x.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(BigbankActivity.this,"BRAVO", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(), BigbankActivity.class));
                overridePendingTransition(R.anim.slide_in_from_top, R.anim.slide_out_to_bottom);
            }
        });



        ImageView ok = (ImageView) this.findViewById(R.id.ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String classe = InformationsCart.getClasse();
                if (classe == "FactureTransfer") {
                    startActivity(new Intent(getApplicationContext(), FactureTransfer.class));
                    overridePendingTransition(R.anim.slide_in_from_top, R.anim.slide_out_to_bottom);
                }
                else
                {
                    startActivity(new Intent(getApplicationContext(), FactureActivity.class));
                    overridePendingTransition(R.anim.slide_in_from_top, R.anim.slide_out_to_bottom);
                }
            }
        });

    }

}