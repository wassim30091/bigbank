package com.wwsight.bigbank.Activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.wwsight.bigbank.Jobs.InformationsCart;
import com.wwsight.bigbank.R;

import java.util.ArrayList;

public class FactureTransfer extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_facture_transfer);


        TableLayout tableLayout = (TableLayout) findViewById(R.id.tableFacture);

        TableRow row = getTableRow( "Frend Loan Back", InformationsCart.getTotalPrice() + " $");
        tableLayout.addView(row);
        String tot = InformationsCart.getTotalPrice();
        TableRow vide = getTableRowVide("", "");


        TextView nom = (TextView) this.findViewById(R.id.Nom);
        nom.setText("Transfer to : Mr " + InformationsCart.getNameApp());


        TableRow total = getTableRowTotal( "TOTAL", "-" + tot + " $");

        tableLayout.addView(vide);
        tableLayout.addView(total);
        ImageView x = (ImageView) this.findViewById(R.id.retour);

        x.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(BigbankActivity.this,"BRAVO", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(), BigbankActivity.class));
                overridePendingTransition(R.anim.slide_in_from_bottom, R.anim.slide_out_to_top);
            }
        });

    }


    public TableRow getTableRow(String title,String price){


        final float scale = getResources().getDisplayMetrics().density;
        int padding_1dp = (int) (1 * scale + 0.5f);
        int padding_2dp = (int) (2 * scale + 0.5f);
        int height_15dp = (int) (15 * scale + 0.5f);
        int widht_0dp = (int) (0 * scale + 0.5f);
        int taille_6dp = (int) (6 * scale + 0.5f);
        int taille_8dp = (int) (115 * scale + 0.5f);

        TableRow tableRow = new TableRow(this);
        TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
        tableRow.setBackgroundColor(Color.BLACK);
        layoutParams.setMargins(1, 1, 1, 1);


        TextView t1 = new TextView(this);
        t1.setLayoutParams(new LinearLayout.LayoutParams(padding_1dp, padding_1dp));
        t1.setPadding(padding_1dp, padding_1dp, padding_1dp, padding_1dp);
        t1.setHeight(height_15dp);
        t1.setWidth(taille_8dp);
        t1.setText(title);
        t1.setTextColor(Color.BLACK);
        t1.setBackgroundColor(Color.WHITE);
        t1.setTextSize(6);
        t1.setTypeface(Typeface.defaultFromStyle(Typeface.NORMAL));
        t1.setGravity(Gravity.CENTER);
        tableRow.addView(t1, layoutParams);


        TextView t2 = new TextView(this);
        t2.setLayoutParams(new LinearLayout.LayoutParams(padding_1dp, padding_1dp));
        t2.setPadding(padding_1dp, padding_1dp, padding_1dp, padding_1dp);
        t2.setHeight(height_15dp);
        t2.setWidth(widht_0dp);
        t2.setText(price);
        t2.setTextColor(Color.BLACK);
        t2.setBackgroundColor(Color.WHITE);
        t2.setTextSize(6);
        t2.setGravity(Gravity.CENTER);
        tableRow.addView(t2 , layoutParams);

        return tableRow;

    }





    public TableRow getTableRowTotal(String title,String price){

        final float scale = getResources().getDisplayMetrics().density;
        int padding_1dp = (int) (1 * scale + 0.5f);
        int padding_2dp = (int) (2 * scale + 0.5f);
        int height_15dp = (int) (15 * scale + 0.5f);
        int widht_0dp = (int) (0 * scale + 0.5f);
        int taille_6dp = (int) (6 * scale + 0.5f);
        int taille_8dp = (int) (115 * scale + 0.5f);

        TableRow tableRow = new TableRow(this);
        TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
        tableRow.setBackgroundColor(Color.BLACK);
        layoutParams.setMargins(1, 1, 1, 1);


        TextView t1 = new TextView(this);
        t1.setLayoutParams(new LinearLayout.LayoutParams(padding_1dp, padding_1dp));
        t1.setPadding(padding_1dp, padding_1dp, padding_1dp, padding_1dp);
        t1.setHeight(height_15dp);
        t1.setWidth(widht_0dp);
        t1.setText(title);
        t1.setTextColor(Color.BLACK);
        t1.setBackgroundColor(Color.WHITE);
        t1.setTextSize(8);
        t1.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
        t1.setGravity(Gravity.CENTER);
        tableRow.addView(t1 , layoutParams);


        TextView t2 = new TextView(this);
        t2.setLayoutParams(new LinearLayout.LayoutParams(padding_1dp, padding_1dp));
        t2.setPadding(padding_1dp, padding_1dp, padding_1dp, padding_1dp);
        t2.setHeight(height_15dp);
        t2.setWidth(widht_0dp);
        t2.setText(price);
        t2.setTextColor(Color.BLACK);
        t2.setBackgroundColor(Color.WHITE);
        t2.setTextSize(8);
        t2.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
        t2.setGravity(Gravity.CENTER);
        tableRow.addView(t2 , layoutParams);


        return tableRow;
    }

    public TableRow getTableRowVide(String title,String price){

        final float scale = getResources().getDisplayMetrics().density;
        int padding_1dp = (int) (1 * scale + 0.5f);
        int padding_2dp = (int) (2 * scale + 0.5f);
        int height_15dp = (int) (15 * scale + 0.5f);
        int height_5dp = (int) (5 * scale + 0.5f);
        int widht_0dp = (int) (0 * scale + 0.5f);
        int taille_6dp = (int) (6 * scale + 0.5f);
        int taille_8dp = (int) (8 * scale + 0.5f);

        TableRow tableRow = new TableRow(this);
        TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
        tableRow.setBackgroundColor(Color.BLACK);
        layoutParams.setMargins(1, 1, 1, 1);


        TextView t1 = new TextView(this);
        t1.setLayoutParams(new LinearLayout.LayoutParams(padding_1dp, padding_1dp));
        t1.setPadding(padding_1dp, padding_1dp, padding_1dp, padding_1dp);
        t1.setHeight(height_5dp);
        t1.setWidth(taille_8dp);
        t1.setText(title);
        t1.setTextColor(Color.BLACK);
        t1.setBackgroundColor(Color.WHITE);
        t1.setTextSize(8);
        t1.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
        t1.setGravity(Gravity.CENTER);
        tableRow.addView(t1 , layoutParams);



        TextView t2 = new TextView(this);
        t2.setLayoutParams(new LinearLayout.LayoutParams(padding_1dp, padding_1dp));
        t2.setPadding(padding_1dp, padding_1dp, padding_1dp, padding_1dp);
        t2.setHeight(height_5dp);
        t2.setWidth(widht_0dp);
        t2.setText(price);
        t2.setTextColor(Color.BLACK);
        t2.setBackgroundColor(Color.WHITE);
        t2.setTextSize(8);
        t2.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
        t2.setGravity(Gravity.CENTER);
        tableRow.addView(t2 , layoutParams);


        return tableRow;
    }




}
