package com.wwsight.bigbank.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.content.Intent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.wwsight.bigbank.R;


public class BigbankActivity extends Activity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_bigbank);


        LinearLayout transf = (LinearLayout) this.findViewById(R.id.menu1);

        transf.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Toast.makeText(BigbankActivity.this, "BRAVO", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(), ProfilActivity.class));
                overridePendingTransition(R.anim.slide_in_from_bottom, R.anim.slide_out_to_top);
            }
        });


        LinearLayout pay = (LinearLayout) this.findViewById(R.id.menu2);

        pay.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Toast.makeText(BigbankActivity.this,"BRAVO", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(), PaymentsActivity.class));
                overridePendingTransition(R.anim.slide_in_from_bottom, R.anim.slide_out_to_top);
            }
        });

        LinearLayout mouvement = (LinearLayout) this.findViewById(R.id.menu3);

        mouvement.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Toast.makeText(BigbankActivity.this,"pas encore", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(), Historique.class));
                overridePendingTransition(R.anim.slide_in_from_bottom, R.anim.slide_out_to_top);
            }
        });

        LinearLayout menu4 = (LinearLayout) this.findViewById(R.id.menu4);

        menu4.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //Toast.makeText(BigbankActivity.this,"pas encore", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(BigbankActivity.this, FactureTransfer.class));
               overridePendingTransition(R.anim.slide_in_from_bottom, R.anim.slide_out_to_top);
            }
        });

        ImageView aide = (ImageView) this.findViewById(R.id.aide);

        aide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(BigbankActivity.this,"BRAVO", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(), AideActivity.class));
                overridePendingTransition(R.anim.slide_in_from_bottom, R.anim.slide_out_to_top);
            }
        });

        ImageView profil = (ImageView) this.findViewById(R.id.prof);

        profil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // Toast.makeText(BigbankActivity.this,"pas encore", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(), ProfilActivity.class));
              overridePendingTransition(R.anim.slide_in_from_bottom, R.anim.slide_out_to_top);
            }
        });

    }


}
