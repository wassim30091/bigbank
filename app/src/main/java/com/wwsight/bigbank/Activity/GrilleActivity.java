package com.wwsight.bigbank.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.wwsight.bigbank.Jobs.InformationsCart;
import com.wwsight.bigbank.R;


public class GrilleActivity extends Activity {

    public static String color;
    //pour g�rer le bouton OK dans l'�cran de la grille
    public static ImageView okButton;
    public static  boolean drawnSymbol = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent myIntent = getIntent(); // gets the previously created intent
        this.color = myIntent.getStringExtra("color"); // will return "FirstKeyValue"
        setContentView(R.layout.activity_grille);

        TextView priceText = (TextView) findViewById(R.id.price);
        TextView nameAppText = (TextView) findViewById(R.id.nameApp);
        TextView commentsText = (TextView) findViewById(R.id.comments);

        priceText.setText(InformationsCart.getTotalPrice()+" $");
        nameAppText.setText(InformationsCart.getNameApp());
        commentsText.setText(InformationsCart.getComments());

        View ViewRose = (View) this.findViewById(R.id.ViewRose);
        View ViewVert = (View) this.findViewById(R.id.ViewVert);
        View ViewBleu = (View) this.findViewById(R.id.ViewBleu);
        View ViewJaune = (View) this.findViewById(R.id.ViewJaune);


        if (this.color.equals("#f0308f")) {
            ViewRose.setBackgroundResource(R.drawable.circle_rose);
            ViewVert.setBackgroundResource(R.drawable.circle_vert_desc);
            ViewBleu.setBackgroundResource(R.drawable.circle_bleu_desc);
            ViewJaune.setBackgroundResource(R.drawable.circle_dore_desc);
        }

        if (this.color.equals("#2de56d")) {
            ViewVert.setBackgroundResource(R.drawable.circle_vert);
            ViewRose.setBackgroundResource(R.drawable.circle_rose_desc);
            ViewBleu.setBackgroundResource(R.drawable.circle_bleu_desc);
            ViewJaune.setBackgroundResource(R.drawable.circle_dore_desc);
        }

        if(this.color.equals("#1e56f3")) {
            ViewBleu.setBackgroundResource(R.drawable.circle_bleu);
            ViewVert.setBackgroundResource(R.drawable.circle_vert_desc);
            ViewJaune.setBackgroundResource(R.drawable.circle_dore_desc);
            ViewRose.setBackgroundResource(R.drawable.circle_rose_desc);
        }

        if (this.color.equals("#e1a729")) {
            ViewJaune.setBackgroundResource(R.drawable.circle_dore);
            ViewVert.setBackgroundResource(R.drawable.circle_vert_desc);
            ViewBleu.setBackgroundResource(R.drawable.circle_bleu_desc);
            ViewRose.setBackgroundResource(R.drawable.circle_rose_desc);
        }

        ImageView x = (ImageView) this.findViewById(R.id.X);
        x.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(BigbankActivity.this,"BRAVO", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(), BigbankActivity.class));
                overridePendingTransition(R.anim.slide_in_from_top, R.anim.slide_out_to_bottom);
            }
        });

        ImageView backButton = (ImageView) this.findViewById(R.id.r);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                if (drawnSymbol) {
                    startActivity(getIntent());
                }
            }
        });

        okButton = (ImageView) this.findViewById(R.id.ok);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawnSymbol){
                    startActivity(new Intent(getApplicationContext(), ValideActivity.class));
                    overridePendingTransition(R.anim.slide_in_from_top, R.anim.slide_out_to_bottom);
                }
            }
        });
    }
}