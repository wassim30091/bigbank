package com.wwsight.bigbank.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.wwsight.bigbank.R;


public class PaymentsActivity extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payments);


        ImageView x = (ImageView) this.findViewById(R.id.X);

        x.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(BigbankActivity.this,"BRAVO", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(), BigbankActivity.class));
                overridePendingTransition(R.anim.slide_in_from_top, R.anim.slide_out_to_bottom);
            }
        });

    }


}
