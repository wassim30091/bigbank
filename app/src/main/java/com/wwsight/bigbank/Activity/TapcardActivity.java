package com.wwsight.bigbank.Activity;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NfcAdapter;
import android.nfc.tech.IsoDep;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ingenico.nfcreader.CardAnalyst;
import com.ingenico.nfcreader.ReaderNfc;
import com.wwsight.bigbank.Jobs.InformationsCart;
import com.wwsight.bigbank.R;

import java.util.Arrays;
import java.util.HashMap;


public class TapcardActivity extends Activity {

    /* -------- NFC Reader variables -------*/
    final private String Tag = "readernfc" ;
    private NfcAdapter mNfcAdapter;
    private static final IntentFilter[] INTENT_FILTER = new IntentFilter[] { new IntentFilter(NfcAdapter.ACTION_TECH_DISCOVERED) };
    private static final String[][] TECH_LIST = new String[][] { { IsoDep.class.getName() } };
    PendingIntent mPendingIntent ;
    LinearLayout linlaHeaderProgress = null;
    ImageView tapCard1Image = null;
    ImageView tapCard2Image = null;
    //--------------------------------------*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tapcard);
        linlaHeaderProgress = (LinearLayout) findViewById(R.id.linlaHeaderProgress);
        tapCard1Image = (ImageView) findViewById(R.id.tapCard1);
        tapCard2Image = (ImageView) findViewById(R.id.tapCard2);

        Intent myIntent = getIntent();
        if(myIntent.getStringExtra("price") != null){
            InformationsCart.setTotalPrice(myIntent.getStringExtra("price"));
        }
        if(myIntent.getStringExtra("nameApp") != null) {
            InformationsCart.setNameApp(myIntent.getStringExtra("nameApp"));
        }
        if(myIntent.getStringExtra("comments") != null){
            InformationsCart.setComments(myIntent.getStringExtra("comments"));
        }
        if(myIntent.getStringArrayExtra("products") != null){
            InformationsCart.setProducts(myIntent.getStringArrayExtra("products"));
        }

        TextView priceText = (TextView) findViewById(R.id.price);
        TextView nameAppText = (TextView) findViewById(R.id.nameApp);
        TextView commentsText = (TextView) findViewById(R.id.comments);

        priceText.setText(InformationsCart.getTotalPrice()+" $");
        nameAppText.setText(InformationsCart.getNameApp());
        commentsText.setText(InformationsCart.getComments());

        ImageView x = (ImageView) this.findViewById(R.id.X);

        x.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*//Toast.makeText(BigbankActivity.this,"BRAVO", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(), BigbankActivity.class));
                overridePendingTransition(R.anim.slide_in_from_top, R.anim.slide_out_to_bottom);*/
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.setComponent(new ComponentName("com.wwsight.manteaux", "com.wwsight.manteaux.paymentActivity"));
                intent.putExtra("payement", "successful");
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_from_bottom, R.anim.slide_out_to_top);
            }
        });

        /*ImageView ok = (ImageView) this.findViewById(R.id.ok);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(BigbankActivity.this,"BRAVO", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(), ChooseColorActivity.class));
                overridePendingTransition(R.anim.slide_in_from_top, R.anim.slide_out_to_bottom);
            }
        });*/

        /*----- NFC reader intent creation ---*/
        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
        mPendingIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, this.getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        //------------------------------------*/

    }

    protected void onPause(){
        super.onPause();

        //loading bar
        linlaHeaderProgress.setVisibility(View.VISIBLE);
        tapCard1Image.setVisibility(View.GONE);
        tapCard2Image.setVisibility(View.GONE);

        if (mNfcAdapter != null) {
            mNfcAdapter.disableForegroundDispatch(this);
        }
    }

    protected void onResume() {
        super.onResume() ;
        if (mNfcAdapter != null) {
            mNfcAdapter.enableForegroundDispatch(this, mPendingIntent, INTENT_FILTER, TECH_LIST);
        }
    }

    /*----- NFC get action ------*/
    protected void onNewIntent(final Intent intent) {
        ReaderNfc sender = new ReaderNfc(intent) ;

        HashMap<Integer, byte[]> card = CardAnalyst.readCard(sender) ;

        byte bpan[] = card.get(0x5A) ; // PAN
        String pan = "";
        if(bpan != null){
            pan = CardAnalyst.hextostring(bpan) ;
        }else{
            byte biso2[] = card.get(0x57) ; // Pan from ISO 2
            if (biso2 != null){
                biso2 = Arrays.copyOfRange(biso2, 0, 8) ;
                pan = CardAnalyst.hextostring(biso2) ;
            }
        }

        String dateexp = "";
        byte bdatexp[] =  card.get(0x5F24) ; // Expiration Date
        if(bdatexp != null){
            dateexp =  CardAnalyst.hextostring(bdatexp).replace(" ", "/").substring(0, 8) ;
        }

        String crytogram = "";
        byte bcrytogram[] =  card.get(0x9F26) ; // Application cryptogramme
        if(bcrytogram != null){
            crytogram = CardAnalyst.hextostring(bcrytogram) ;
        }


        System.out.println("PAN : " + pan);
        System.out.println("Date Expiration : " + dateexp ) ;
        System.out.println("Application cryptogram : " + crytogram ) ;

        linlaHeaderProgress.setVisibility(View.GONE);
        tapCard1Image.setVisibility(View.VISIBLE);
        tapCard2Image.setVisibility(View.VISIBLE);

        startActivity(new Intent(getApplicationContext(), ChooseColorActivity.class));
        //overridePendingTransition(R.anim.slide_in_from_top, R.anim.slide_out_to_bottom);

    }
}
