package com.wwsight.bigbank.Jobs;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by labiadh on 08/07/2015.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    // Logcat tag
    private static final String LOG = "DatabaseHelper";

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "Bigbank";

    // Table Names
    private static final String TABLE_MOVEMENT = "movements";

    private static final String KEY_ID          = "id";
    private static final String KEY_CREATED_AT  = "created_at";
    private static final String KEY_OPERATION   = "operation";
    private static final String KEY_TITLE       = "title";
    private static final String KEY_DESCRIPTION = "description";
    private static final String KEY_PRICE       = "price";

    // Table Create Statements
    // Todo table create statement
    private static final String CREATE_TABLE_MOVEMENTS = "CREATE TABLE "
            + TABLE_MOVEMENT + "(" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_OPERATION
            + " TEXT," + KEY_TITLE + " TEXT," + KEY_DESCRIPTION + " TEXT," + KEY_PRICE + " TEXT," + KEY_CREATED_AT
            + " DATETIME" + ")";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        // creating required tables
        db.execSQL(CREATE_TABLE_MOVEMENTS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // on upgrade drop older tables
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MOVEMENT);
        // create new tables
        onCreate(db);
    }

    /*
 * Creating a todo
 */
    public long createToDo(InformationsCart elemt) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_OPERATION,   elemt.getOperation());
        values.put(KEY_TITLE,       elemt.getNameApp());
        values.put(KEY_DESCRIPTION, elemt.getComments());
        values.put(KEY_PRICE,       elemt.getTotalPrice());
        values.put(KEY_CREATED_AT,  getDateCreation());

        // insert row
        long movement_id = db.insert(TABLE_MOVEMENT, null, values);

        values.put(KEY_ID, movement_id);
        long id = db.insert(TABLE_MOVEMENT, null, values);

        return movement_id;
    }

    // closing database
    public void closeDB() {
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null && db.isOpen())
            db.close();
    }

    /**
     * getting all tags
     * */
    public List<String> getAllTags() {
        List<InformationsCart> tags = new ArrayList<InformationsCart>();
        String selectQuery = "SELECT  * FROM " + TABLE_MOVEMENT;

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                InformationsCart t = new InformationsCart();
                t.setId(c.getInt((c.getColumnIndex(KEY_ID))));
                t.setTagName(c.getString(c.getColumnIndex(KEY_TAG_NAME)));

                // adding to tags list
                tags.add(t);
            } while (c.moveToNext());
        }
        return tags;
    }
}
