package com.wwsight.bigbank.Jobs;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by labiadh on 02/07/2015.
 */
public class InformationsCart {

    private static String nameApp;
    private static String totalPrice;
    private static String comments;
    private static String classe;
    private static ArrayList<String[]> products;

    public static String getNameApp() {
        return nameApp;
    }

    public static void setNameApp(String nameApp) {
        InformationsCart.nameApp = nameApp;
    }

    public static String getClasse() {
        return classe;
    }

    public static void setClasse(String classe) {
        InformationsCart.classe = classe;
    }

    public static String getTotalPrice() {
        return totalPrice;
    }

    public static void setTotalPrice(String totalPrice) {
        InformationsCart.totalPrice = totalPrice;
    }

    public static String getComments() {
        return comments;
    }

    public static void setComments(String comments) {
        InformationsCart.comments = comments;
    }

    public static ArrayList<String[]> getProducts() {
        return products;
    }

    public static void setProducts(String[] productsArray) {
        products = new ArrayList<String[]>();
        for(int i = 0; i < productsArray.length; i++){
            products.add(productsArray[i].split("//"));
        }

    }
}
