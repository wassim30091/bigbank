package com.ingenico.nfcreader;

import java.math.BigInteger;

class Util {
	
	final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();

	public static byte[] String_to_Bytes(String s) {

		if (s == null || s.length() < 2)
			return null;

		return new BigInteger(s.replace(" ", ""),16).toByteArray();
	}

	public static String Bytes_to_Hex(byte[] bytes) {
		char[] hexChars = new char[bytes.length * 3];
		for ( int j = 0; j < bytes.length; j++ ) {
			int v = bytes[j] & 0xFF;
			hexChars[j * 3] = hexArray[v >>> 4];
	 		hexChars[j * 3 + 1] = hexArray[v & 0x0F];
	 		hexChars[j * 3 + 2] = 0x20;
		}
		return new String(hexChars);
	}
	public static int Byte_to_Int(byte b) {
		if (b < 0)
			return (int) b + 256;
		return b; 
	}
	public static String Int_to_Hex(int i) {

		return String.format("%02X", i);
	}
	
}