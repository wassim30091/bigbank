package com.ingenico.nfcreader;

import java.util.Arrays;
import java.util.Map;


public class ProtFrame {

	
	final protected static Integer[] TAG2B = {0x5F , 0x9F, 0xBF , 0xDF};


	final protected static Integer[] CONT = {0x61, 0x6F, 0x70, 0x73, 0x77, 0xA5, 0x80, 0xBF0C};

	public static String getApduCmd(byte[] buf) {

		if (buf == null){
			return "" ;
		}
		
		int lenBuf = buf.length;
		if (lenBuf < 2)
			return "";


		int iTag = (Util.Byte_to_Int(buf[0]) << 8) |  Util.Byte_to_Int(buf[1]);

		switch (iTag) {
		case 0x0020:
			return "VERIFY";
		case 0x0082:
			return "EXTERNAL AUTHENTICATE";
		case 0x0084:
			return "GET CHALLENGE";
		case 0x0088:
			return "INTERNAL AUTHENTICATE";
		case 0x00A4:
			return "SELECT";
		case 0x00B2:
			return "READ RECORD";
		case 0x8016:
			return "CARD BLOCK";
		case 0x8018:
			return "APPLICATION UNBLOCK";
		case 0x801E:
			return "APPLICATION BLOCK";
		case 0x8024:
			return "PERSONAL IDENTIFICATION NUMBER (PIN) CHANGE/UNBLOCK";
		case 0x80A8:
			return "GET PROCESSING OPTIONS";
		case 0x80AE:
			return "GENERATE APPLICATION CRYPTOGRAM";
		case 0x80CA:
			return "GET DATA";
		}
		return "";
	}	

	public static byte[] createGpo(byte pdol[] , Map<Integer,byte[]> card ){
		
		byte gpo[] =  new byte[] { (byte)0x80, (byte)0xA8 , 0x00 , 0x00 , 0x02 , (byte)0x83 , 0x00 } ;
		
		int i = 0 ;
		int len = 0 ;
		if (pdol != null){
			while (i < pdol.length) {
				int tag =Util.Byte_to_Int(pdol[i]) ;
				if (Arrays.asList(TAG2B).contains(tag)) {
					i++;
					tag = tag*256 + Util.Byte_to_Int(pdol[i]) ;
				}
				i++;
			
				int lenElem = Util.Byte_to_Int(pdol[i]);
				i++ ;
				gpo = Arrays.copyOf(gpo , gpo.length + lenElem ) ;
				byte tmp[] =null  ;
				if( card.get(tag) != null ){
					tmp = card.get(tag) ;
				}else{
					tmp = getTagDefautValue(tag) ;
				}
				if(tmp != null){
				
					for(int j=0 ; j<tmp.length && j<lenElem ; j++) {
						gpo[7+len+j] = tmp[j] ;
					}
				}
				len +=  lenElem ;
			}
		}
		gpo[6] = (byte) len ;
		gpo[4] = (byte) (len+2) ;
		
		gpo = Arrays.copyOf(gpo , gpo.length + 1 ) ;
		
		return gpo ;
	}

public static byte[] createGAC(byte cdol[] , Map<Integer,byte[]> card ){
		
		byte gac[] =  new byte[] { (byte)0x80, (byte)0xAE , (byte)0x40 , 0x00 , 0x00 } ;
		
		int i = 0 ;
		int len = 0 ;
		if (cdol != null){
			while (i < cdol.length) {
				int tag =Util.Byte_to_Int(cdol[i]) ;
				if (Arrays.asList(TAG2B).contains(tag)) {
					i++;
					tag = tag*256 + Util.Byte_to_Int(cdol[i]) ;
				}
				i++;
			
				int lenElem = Util.Byte_to_Int(cdol[i]);
				i++ ;
				gac = Arrays.copyOf(gac , gac.length + lenElem ) ;
				byte tmp[] =null  ;
				if( card.get(tag) != null ){
					tmp = card.get(tag) ;
				}else{
					tmp = getTagDefautValue(tag) ;
				}
				if(tmp != null){
					for(int j=0 ; j<tmp.length && j<lenElem ; j++) {
						gac[5+len+j] = tmp[j] ;
					}
				}
				len +=  lenElem ;
			}
		}

		gac[4] = (byte) (len) ;		
		gac = Arrays.copyOf(gac , gac.length + 1 ) ; // add a zero to end
		
		return gac ;
	}

	
	

	public static String getApdu(byte[] buf, int iLevel, Map<Integer,byte[]> card) {
		
		String sOut = "";
		String sMarge = "\n          ";
		for (int l = 0; l < iLevel; l++)
			sMarge += "    ";


		if(buf == null){
			return "" ;
		}
		
		int i = 0;
		int iTag = Util.Byte_to_Int(buf[i]);

		int lenBuf = buf.length;
		//System.out.println(String.format("\n >>> getApdu: lenBuf = %d, buf = %s\n", lenBuf, Util.Bytes_to_Hex(buf)));
		
		while (i < lenBuf) {
			iTag = Util.Byte_to_Int(buf[i]);
			//System.out.println(String.format("i = %d, iTag = %s", i, Util.Int_to_Hex(iTag)));
			
			if (Arrays.asList(TAG2B).contains(iTag)) {
				i++;
				iTag = iTag*256 + Util.Byte_to_Int(buf[i]) ;
			}
			i++;
			if (i > lenBuf) {
				sOut += sMarge + String.format("## INCORRECT LENGTH %d", i);
				break;
			}
			int lenElem = Util.Byte_to_Int(buf[i]); 
			if (lenElem == 0x81) {
				i++;
				lenElem = Util.Byte_to_Int(buf[i]) ;
			}

			sOut += sMarge + Util.Int_to_Hex(iTag) + " :  " + ProtFrame.getApduTag(iTag) + " (length = " + Util.Int_to_Hex(lenElem) + ")";
		
			int iMin = i + 1;
			int iMax = iMin + lenElem;
			if (iMax > lenBuf) {
				sOut += sMarge + String.format("## INCORRECT LENGTH %d", lenBuf);
				break;
			}
			//System.out.println(String.format("iTag=%s, lenElem = %s, iMin = %d, iMax = %d", Util.Int_to_Hex(iTag), Util.Int_to_Hex(lenElem), iMin, iMax));
			//int lenBufSub = iMax - iMin + 1;
			byte[] bufSub = new byte[lenElem];
			System.arraycopy(buf, iMin, bufSub, 0, lenElem);

			if (Arrays.asList(CONT).contains(iTag)) {
				//System.out.println("Tag in CONT");
				sOut += ProtFrame.getApdu(bufSub, iLevel + 1, card);
			}
			else
				sOut += sMarge + "      DATA : " + Util.Bytes_to_Hex(bufSub);
				card.put(iTag, bufSub) ;
			i = iMax;

		}
		return sOut + "\n\n";
	}

	public static String getApduTag(int iFirst) {

		switch(iFirst) {
		case 0x42: return "Issuer Identification Number (IIN)";
		case 0x4F: return "Application Identifier (AID) - card";
		case 0x50: return "Application Label";
		case 0x57: return "Track 2 Equivalent Data";
		case 0x5A: return "Application Primary Account Number (PAN)";
		case 0x5F20: return "Cardholder Name";
		case 0x5F24: return "Application Expiration Date";
		case 0x5F25: return "Application Effective Date";
		case 0x5F28: return "Issuer Country Code";
		case 0x5F2A: return "Transaction Currency Code";
		case 0x5F2D: return "Language Preference";
		case 0x5F30: return "Service Code";
		case 0x5F34: return "Application Primary Account Number (PAN) Sequence Number";
		case 0x5F36: return "Transaction Currency Exponent";
		case 0x5F50: return "Issuer URL";
		case 0x5F53: return "International Bank Account Number (IBAN)";
		case 0x5F54: return "Bank Identifier Code (BIC)";
		case 0x5F55: return "Issuer Country Code (alpha2 format)";
		case 0x5F56: return "Issuer Country Code (alpha3 format)";
		case 0x61: return "Application Template ";
		case 0x6F: return "File Control Information (FCI) Template"; 
		case 0x70: return "READ RECORD Response Message Template";
		case 0x71: return "Issuer Script Template 1";
		case 0x72: return "Issuer Script Template 2";
		case 0x73: return "Directory Discretionary Template";
		case 0x77: return "Response Message Template Format 2";
		case 0x80: return "Response Message Template Format 1";
		case 0x81: return "Amount, Authorised (Binary)";
		case 0x82: return "Application Interchange Profile";
		case 0x83: return "Command Template";
		case 0x84: return "Dedicated File (DF) Name";
		case 0x86: return "Issuer Script Command";
		case 0x87: return "Application Priority Indicator";
		case 0x88: return "Short File Identifier (SFI)";
		case 0x89: return "Authorisation Code";
		case 0x8A: return "Authorisation Response Code";
		case 0x8C: return "Card Risk Management Data Object List 1 (CDOL1)";
		case 0x8D: return "Card Risk Management Data Object List 2 (CDOL2)";
		case 0x8E: return "Cardholder Verification Method (CVM) List";
		case 0x8F: return "Certification Authority Public Key Index";
		case 0x90: return "Issuer Public Key Certificate";
		case 0x91: return "Issuer Authentication Data";
		case 0x92: return "Issuer Public Key Remainder";
		case 0x93: return "Signed Static Application Data";
		case 0x94: return "Application File Locator (AFL)";
		case 0x95: return "Terminal Verification Results";
		case 0x97: return "Transaction Certificate Data Object List (TDOL)";
		case 0x98: return "Transaction Certificate (TC) Hash Value";
		case 0x99: return "Transaction Personal Identification Number (PIN) Data";
		case 0x9A: return "Transaction Date";
		case 0x9B: return "Transaction Status Information";
		case 0x9C: return "Transaction Type";
		case 0x9D: return "Directory Definition File (DDF) Name";
		case 0x9F01: return "Acquirer Identifier";
		case 0x9F02: return "Amount, Authorised (Numeric)";
		case 0x9F03: return "Amount, Other (Numeric)";
		case 0x9F04: return "Amount, Other (Binary)";
		case 0x9F05: return "Application Discretionary Data";
		case 0x9F06: return "Application Identifier (AID) - terminal";
		case 0x9F07: return "Application Usage Control";
		case 0x9F08: return "Application Version Number";
		case 0x9F09: return "Application Version Number";
		case 0x9F0B: return "Cardholder Name Extended";
		case 0x9F0D: return "Issuer Action Code - Default";
		case 0x9F0E: return "Issuer Action Code - Denial";
		case 0x9F0F: return "Issuer Action Code - Online";
		case 0x9F10: return "Issuer Application Data";
		case 0x9F11: return "Issuer Code Table Index";
		case 0x9F12: return "Application Preferred Name";
		case 0x9F13: return "Last Online Application Transaction Counter (ATC) Register";
		case 0x9F14: return "Lower Consecutive Offline Limit";
		case 0x9F15: return "Merchant Category Code";
		case 0x9F16: return "Merchant Identifier";
		case 0x9F17: return "Personal Identification Number (PIN) Try Counter";
		case 0x9F18: return "Issuer Script Identifier";
		case 0x9F1A: return "Terminal Country Code";
		case 0x9F1B: return "Terminal Floor Limit";
		case 0x9F1C: return "Terminal Identification";
		case 0x9F1D: return "Terminal Risk Management Data";
		case 0x9F1E: return "Interface Device (IFD) Serial Number";
		case 0x9F1F: return "Track 1 Discretionary Data";
		case 0x9F20: return "Track 2 Discretionary Data";
		case 0x9F21: return "Transaction Time";
		case 0x9F22: return "Certification Authority Public Key Index";
		case 0x9F23: return "Upper Consecutive Offline Limit";
		case 0x9F26: return "Application Cryptogram";
		case 0x9F27: return "Cryptogram Information Data";
		case 0x9F2D: return "ICC PIN Encipherment Public Key Certificate";
		case 0x9F2E: return "ICC PIN Encipherment Public Key Exponent";
		case 0x9F2F: return "ICC PIN Encipherment Public Key Remainder";
		case 0x9F32: return "Issuer Public Key Exponent";
		case 0x9F33: return "Terminal Capabilities";
		case 0x9F34: return "Cardholder Verification Method (CVM) Results";
		case 0x9F35: return "Terminal Type";
		case 0x9F36: return "Application Transaction Counter (ATC)";
		case 0x9F37: return "Unpredictable Number";
		case 0x9F38: return "Processing Options Data Object List (PDOL)";
		case 0x9F39: return "Point-of-Service (POS) Entry Mode";
		case 0x9F3A: return "Amount, Reference Currency";
		case 0x9F3B: return "Application Reference Currency";
		case 0x9F3C: return "Transaction Reference Currency Code";
		case 0x9F3D: return "Transaction Reference Currency Exponent";
		case 0x9F40: return "Additional Terminal Capabilities";
		case 0x9F41: return "Transaction Sequence Counter";
		case 0x9F42: return "Application Currency Code";
		case 0x9F43: return "Application Reference Currency Exponent";
		case 0x9F44: return "Application Currency Exponent";
		case 0x9F45: return "Data Authentication Code";
		case 0x9F46: return "ICC Public Key Certificate";
		case 0x9F47: return "ICC Public Key Exponent";
		case 0x9F48: return "ICC Public Key Remainder";
		case 0x9F49: return "Dynamic Data Authentication Data Object List (DDOL)";
		case 0x9F4A: return "Static Data Authentication Tag List";
		case 0x9F4B: return "Signed Dynamic Application Data";
		case 0x9F4C: return "ICC Dynamic Number";
		case 0x9F4D: return "Log Entry";
		case 0x9F4E: return "Merchant Name and Location";
		case 0x9F4F: return "Log Format";			
		case 0xA5: return "File Control Information (FCI) Proprietary Template";
		case 0xBF0C: return "File Control Information (FCI) Issuer Discretionary Data";
		case 0xDF4B: return "POS Cardholder Interaction Information";
		}
		return String.format("# TAG NOT FOUND: %X", iFirst);
	}

	private static byte[] getTagDefautValue(int iFirst) {

		switch(iFirst) {
		
		case 0x5f2a : return new byte[] { 0x09 , 0x78 }  ;
		case 0x95 :   return new byte[] { 0x00 , 0x00 , 0x00 , 0x00 , 0x00 }  ;
		case 0x9a :   return new byte[] { 0x15 , 0x02 , 0x24 }  ;
		case 0x9c :   return new byte[] { 0x00 }  ;
		case 0x9f02 : return new byte[] { 0x00, 0x00, 0x00, 0x00, 0x01, 0x00 }  ;
		case 0x9f03 : return new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}  ;
		case 0x9f33 : return new byte[] { (byte) 0xE0, (byte) 0xA0, 0x00 } ;
		case 0x9f35 : return new byte[] { 0x22 }  ;
		case 0x9f37 : return new byte[] { 0x5b , (byte)0x84 , 0x4f , 0x22}  ;
		case 0x9f40 : return new byte[] { (byte) 0x8e, (byte) 0, (byte) 0xb0, 0x50, 0x05 };
		case 0x9f66 : return new byte[] { (byte)0x32 , 0x00 , 0x40 , 0x00 }  ;
		case 0x9f1a : return new byte[] { 0x02 , 0x50 }  ;
		case 0x9f1d : return new byte[] { 0, 0, 0, 0, 0, 0, 20, 0} ;
		case 0x9F45 : return new byte[] { 0, 0 } ;
		case 0x9F4C : return new byte[] { 0, 0, 0, 0, 0, 0, 0, 0} ;
		case 0x9F34 : return new byte[] { 0, 1, 0 } ;

		}
		System.out.println( String.format("Missing Tag %X", iFirst)) ;
		return null ;
	}
	
	
	public static byte[] searchTag(byte[] buf , int tag) {
		

		int i = 0;
		int iTag = Util.Byte_to_Int(buf[i]);
		int lenBuf = buf.length;
	
		while (i < lenBuf) {
			iTag = Util.Byte_to_Int(buf[i]);
			
			if (Arrays.asList(TAG2B).contains(iTag)) {
				i++;
				iTag = iTag*256 + Util.Byte_to_Int(buf[i]) ;
			}
			i++;
			if (i >= lenBuf) {
				break;
			}
			int lenElem = Util.Byte_to_Int(buf[i]); 
			if (lenElem == 0x81) {
				i++;
				lenElem = Util.Byte_to_Int(buf[i]); 
			}
		
			int iMin = i + 1;
			int iMax = iMin + lenElem;
			if (iMax > lenBuf) {
				break;
			}
			//int lenBufSub = iMax - iMin + 1;
			byte[] bufSub = new byte[lenElem];
			System.arraycopy(buf, iMin, bufSub, 0, lenElem);

			
			if( iTag == tag){
				return bufSub ;
			}
			
			if (Arrays.asList(CONT).contains(iTag)) {
				 byte tmp[] = ProtFrame.searchTag(bufSub, tag);
				 if(tmp.length != 0){
					 return tmp ;
				 }
				 
			}
			i = iMax;
		}
		return new byte[0];
	}
}