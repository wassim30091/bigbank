package com.ingenico.nfcreader;

public interface ReaderCardInterface {

	// Active the card and return card ATR.
	public byte[] powerOn() ;
	
	// Send APDU command and return the response.
	public byte[] sendApdu(byte[] apdu) ;
	
	// return true if card is present.
	public boolean iscardpresent() ;
	
	// deactivate the card.
	public void powerOff() ;
	
}
