package com.ingenico.nfcreader;

import android.annotation.SuppressLint;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

@SuppressLint("UseSparseArrays")
public class CardAnalyst {

	
	
	static private byte[] select_df= new byte[] { 0x00, (byte)0xA4, 0x04, 0x00, 0x0E, 0x32, 0x50, 0x41, 0x59, 0x2E, 0x53, 0x59, 0x53, 0x2E, 0x44, 0x44, 0x46, 0x30, 0x31, 0x00 } ;
	
	/* select apdu for MAstercard , Visa and CB */
	static private byte[] select0  = new byte[] { 0x00, (byte)0xA4, 0x04 , 0x00 , 0x07 , 0x00  } ;
	
	static private byte[] select1  = new byte[] { 0x00, (byte)0xA4, 0x04 , 0x00 , 0x07 , (byte)0xA0 , 0x00 , 0x00 , 0x00 , 0x42 , 0x10 , 0x10, 0x00  } ;
	static private byte[] select2  = new byte[] { 0x00, (byte)0xA4, 0x04 , 0x00 , 0x07 , (byte)0xA0 , 0x00 , 0x00 , 0x00 , 0x04 , 0x10 , 0x10, 0x00  } ;
	static private byte[] select3  = new byte[] { 0x00, (byte)0xA4, 0x04 , 0x00 , 0x07 , (byte)0xA0 , 0x00 , 0x00 , 0x00 , 0x03 , 0x10 , 0x10, 0x00  } ;
	static private byte[][] select = new byte[][] { select3 , select2 , select1 } ;
	

	/* read record */
	static private byte readRecord[] = new byte[] { 0x00, (byte)0xB2, 0x00, (byte)0x00, 0x00 } ;
	/* get data */
	static private byte getData[] = new byte[] { (byte)0x80, (byte)0xCA, 0x00, (byte)0x00, 0x00 } ;
	
	
	public static HashMap<Integer,byte[]> readCard(ReaderCardInterface apdusender){
		byte rep[] ;
		byte apdu[] = null;
		byte tag[] ;
		HashMap<Integer,byte[]> card = new HashMap<Integer,byte[]>() ;
		
		
		
		card.put(0x9f02, new byte[] {0x00,0x00,0x00,0x15,0x00 }) ;
		card.put(0x9f37, new byte[] {0x12,0x34,0x56,0x78 }) ;
		
		System.out.println("Send Apdu\napdu: "+hextostring(select_df)) ;
		rep = apdusender.sendApdu(select_df);
		System.out.println("\nrep :" + hextostring(rep)) ;
		
		System.out.println(ProtFrame.getApdu(rep, 0, card)) ;
		
		byte aid[] = card.get(0x4f) ;
		
		if( aid != null){
			apdu = Arrays.copyOf(select0, aid.length+6) ;
			apdu[4] =  (byte) aid.length ;
			System.arraycopy(aid, 0, apdu, 5, aid.length) ;
		}else{
			for(int i = 0 ; i< select.length ; i++) {
				rep = apdusender.sendApdu(select[i]); 
				System.out.println("Send Apdu\napdu: "+hextostring(select[i])+"\nrep :" + hextostring(rep)) ;
				if( rep[rep.length-2] == (byte)0x90 && rep[rep.length-1] == 0 ){
					System.out.println("AID found") ;
					apdu = select[i] ;
				}	
			}
		}
		
		if( apdu == null){
			System.out.println("Pas d'application trouv�");
			return card;
		}
		
		
		
		System.out.println("Send Apdu\napdu: "+hextostring(apdu));
		rep = apdusender.sendApdu(apdu); 
		System.out.println("rep :" + hextostring(rep)) ;
		System.out.println(ProtFrame.getApdu(rep, 0, card)) ;
		
		tag = card.get(0x9f38) ;
		apdu = ProtFrame.createGpo(tag, card) ;
				
		System.out.println("Send Apdu\napdu: "+hextostring(apdu) ) ;
		rep = apdusender.sendApdu(apdu); 
		
		
		if(rep == null || (rep.length == 2 && rep[0] != (byte)0x90) ){
			System.out.println("fail gpo !!") ;
			return card;
		}
		System.out.println("rep :" + hextostring(rep)) ;
		
		
		System.out.println(ProtFrame.getApdu(rep, 0, card)) ;
		
		if(rep[0]== (byte)0x77){
			
			tag = ProtFrame.searchTag(rep, 0x94) ;

		}else if(rep[0]== (byte)0x80){
			
			tag = new byte[(rep.length - 6)] ;
			System.arraycopy(rep, 4, tag, 0, (rep.length - 6));
		}

		if(tag != null && tag.length != 0){
			System.out.println("AFL found : "+hextostring(tag)) ;
		}else{
			return card;
		}
		
		
		Vector<byte[]> files = getfile(tag) ;
		byte apdu2[] = readRecord.clone() ;
		byte t[] = new byte[2] ;
		
		for(Iterator<byte[]> i = files.iterator(); i.hasNext() ;  ){
			t = i.next() ;
			System.out.println("file : "+hextostring(t)) ;
		
			apdu2[2] = t[0] ; 
			apdu2[3] = t[1] ;
			
			rep = apdusender.sendApdu(apdu2); 
			System.out.println("Send Apdu\napdu: "+hextostring(apdu2)+"\nrep :" + hextostring(rep)) ;
			System.out.println(ProtFrame.getApdu(rep, 0, card)) ;
		}
		
		/* get historic transaction */
		byte logEntry[] = card.get(0x9f4D) ;
		if(logEntry != null){
			apdu = readRecord.clone() ;
			apdu[2] = 1 ;
			apdu[3] = (byte) (logEntry[0] * 8 + 4) ;
			System.out.println("Send Apdu\napdu: "+hextostring(apdu)) ;
			rep = apdusender.sendApdu(apdu); 
			System.out.println("rep :" + hextostring(rep)) ;
		
			/* get historic transaction format*/
			apdu = getData.clone() ;
			apdu[2] = (byte)0x9f ;
			apdu[3] = 0x4F ;
			System.out.println("Send Apdu\napdu: "+hextostring(apdu)) ;
			rep = apdusender.sendApdu(apdu); 
			System.out.println("rep :" + hextostring(rep)) ;
		}
		
		// send genetare AC.
		tag = card.get(0x8c) ;
		
		apdu = ProtFrame.createGAC(tag, card) ;
		System.out.println("Send Apdu\napdu: "+hextostring(apdu)) ;
		rep = apdusender.sendApdu(apdu); 
		System.out.println("rep :" + hextostring(rep)) ;
		System.out.println(ProtFrame.getApdu(rep, 0, card)) ;
		
		
		return card ;
		
	}

	public static String hextostring(byte[] tab){
		String s = "" ;
		
		if(tab == null) {return s ; }
		
		for(int i=0 ; i<tab.length ;i++){
			s += String.format("%02X",tab[i] & 0xFF) +" " ;
	    }
	    	
		return s ;
	}
	
	private static Vector<byte[]> getfile(byte afl[]){
		Vector<byte[]> files = new Vector<byte[]>();
		
		for(int i=0 ; 4*i<afl.length ; i++){
			byte a,b,c ;
			a = afl[4*i] ;
			b = afl[4*i+1] ;
			c = afl[4*i+2] ;
			
			for(byte j=b; j<=c ; j++ ){
				byte n[]= new byte[2] ;
				n[0] = j ;
				n[1] = (byte) (a+4);
				files.add(n) ;
			}
		}
		
		return files ;
	}
}
