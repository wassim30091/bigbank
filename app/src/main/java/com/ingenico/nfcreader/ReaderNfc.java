package com.ingenico.nfcreader;

import android.content.Intent;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.IsoDep;

import java.io.IOException;


public class ReaderNfc implements ReaderCardInterface {

	final private static String Tag = "readernfc" ;
	private IsoDep tag ;
	
	
	public ReaderNfc(Intent intent){
		
		Tag tagFromIntent = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
	    tag = IsoDep.get(tagFromIntent);
		
	    try {
			tag.connect();
			tag.setTimeout(1000) ;
	    }catch (IOException e) {
			e.printStackTrace();
	    }
	   
	}
	
	
	public byte[] sendApdu(byte[] apdu){
		
			byte[] rep;
			try {
				//Log.d(Tag, "command :"  + hextostring(apdu) ) ;
				rep = tag.transceive(apdu );
				//Log.d(Tag, "response :"  + hextostring(rep) ) ;
				return rep ;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return null ;
	    
	}
	
	
	public static String hextostring(byte[] tab){
		String s = "" ;
		if( tab == null){
			return s ;
		}
		
		for(int i=0 ; i<tab.length ;i++){
			s += Integer.toHexString(tab[i] & 0xFF) +" " ;
	    }
	    	
		return s ;
	}


	@Override
	public byte[] powerOn() {
		
		return tag.getHistoricalBytes() ;
	}


	@Override
	public boolean iscardpresent() {
		tag.isConnected() ;
		return true;
	}


	@Override
	public void powerOff() {
		
	}
	
}
